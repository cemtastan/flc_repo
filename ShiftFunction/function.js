
function shiftArray(arr,amount) {
    let arrTemp;
    for (let index = 0; index < amount; index++) {
        arrTemp = arr.shift();
        arr.push(arrTemp);
    }
    console.log(arr);
}

function rightShift(arr){
    let arr1;
    arr1 = arr.concat(arr.splice(0,2));
    console.log('Right Shifted array: ',  arr1)
}

function leftShift(arr){
    let arr1;
    arr1 = arr.shift();
    arr.push(arr1);
    console.log('Left Shifted array: ', arr)
}


let data = [ 0, 1, 2, 3, 4, 5, 6 ];

shiftArray(data,4);
leftShift(data);
rightShift(data);